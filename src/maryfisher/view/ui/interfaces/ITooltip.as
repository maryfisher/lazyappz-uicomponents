package maryfisher.view.ui.interfaces {
	
	/**
	 * ...
	 * @author mary_fisher
	 */
	public interface ITooltip {
		function switchVisibility():void;
		function show():void;
		function hide():void;
		
		function destroy():void;
	}
	
}