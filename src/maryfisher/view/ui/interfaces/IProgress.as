package maryfisher.view.ui.interfaces {
	import flash.display.DisplayObject;
	
	/**
	 * ...
	 * @author mary_fisher
	 */
	public interface IProgress {
		function set maskWidth(value:Number):void;
		function get totalWidth():Number;
	}
	
}