package maryfisher.view.ui.interfaces {
	import maryfisher.framework.view.IDisplayObject;
	
	/**
	 * ...
	 * @author mary_fisher
	 */
	public interface IIDItem extends IDisplayObject{
		function get id():String;
	}
	
}